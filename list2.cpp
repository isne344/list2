#include <iostream>
#include "list.h"

List::~List() {
	for(Node *p; !isEmpty(); ){
		p=head->next;
		delete head;
		head=p;
	}
}
void List::headPush(int a) {
	Node *nextnode = new Node(a);
	
	if(!isEmpty()){
		newnode->next = head;
		head = nextnode;
	}
	
	else {
		head = nextnode;
		tail = nextnode;
	}

}
void List::tailPush(int b) {
	Node *nextnode = new Node(b);
	
	if (!isEmpty()) {
		tail->next = nextnode;
		tail = nextnode;
	}
	else {
		head = nextnode;
		tail = nextnode;
	}
}
int List::tailPop()
{
	Node *x = head;
	int a= tail->info;			
	if (isEmpty()) { return NULL; }
	if (!isEmpty() && head != tail) 
	{
		while(x->next->next !=NULL)
		{
			x = x->next;
		}
		tail = x;
		x = tail->next;
		tail->next = NULL;
		delete x;
		return a;
	}
	else if(!isEmpty() && head==tail)
	{
		a = x->info;
		head = x->next;
		delete x;
		return a;
	}
	return NULL;
}
